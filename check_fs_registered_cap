#!/usr/bin/perl -w
#
#       Author: Luis Daniel Lucio Quiroz <dlucio@okay.com.mx>
#       Special thanks to:
#               T5 Telecom Inc. <info@t5tele.com> for sponsoring
#

use POSIX;
use strict;
use Getopt::Long;
use vars qw($opt_V $opt_h $opt_v $verbose $PROGNAME @opt_e $opt_w $opt_c $opt_d $opt_t $opt_fs $command $opt_M $mailq $status $state $msg $msg_q $msg_p $opt_r $opt_C $mailq @lines $_reg_user $_realm $_token $_url $_expires $_network_ip $_network_port $_network_proto $_hostname %srcdomains %dstdomains $e $i $_e $_d);
use lib "/usr/lib/nagios/plugins";
use utils qw(%ERRORS &print_revision &support &usage );

sub print_help ();
sub print_usage ();
sub process_arguments ();

$ENV{'PATH'}='';
$ENV{'BASH_ENV'}=''; 
$ENV{'ENV'}='';
$PROGNAME = "check_fs_register";
$command = "/usr/bin/fs_cli --execute 'show registrations'";
$state = $ERRORS{'UNKNOWN'};
Getopt::Long::Configure('bundling');
$status = process_arguments();
if ($status){
        print "ERROR: processing arguments\n";
        exit $ERRORS{"UNKNOWN"};
}

$SIG{'ALRM'} = sub {
        print ("ERROR: timed out waiting\n");
        exit $ERRORS{"WARNING"};
};


alarm($opt_t);
$i = 0;
foreach (`$command`){
        ($_reg_user,$_realm,$_token,$_url,$_expires,$_network_ip,$_network_port,$_network_proto,$_hostname) = split /,/;
        if (($_reg_user ne 'reg_user') &&  ($_reg_user !~ /\s+/)){

                foreach $e (@opt_e){
                        print "$_reg_user,$_realm,$_token,$_url,$_expires,$_network_ip,$_network_port,$_network_proto,$_hostname " if $verbose;
                        ($_e, $_d) = split (/\@/,$e);
                        if (defined($_d)){
                                if (($_d eq $_realm) and (($_reg_user eq $_e) or ($_e eq '*'))) {
                                        print "matches $e" if $verbose;
                                        $i++;
                                }
                        }
                        elsif (defined($opt_r)){
                                if (($opt_r eq $_realm) and (($_reg_user eq $e) or ($e eq '*'))) {
                                        print "matches $e" if $verbose;
                                        $i++;
                                }
                        }
                        else{
                                if (($_reg_user eq $e) or ($e eq '*')){
                                        print "matches $e" if $verbose;
                                        $i++;
                                }
                        }
                        print "\n" if $verbose;
                }
        }
}

if ($i >= $opt_c){
        print "EXT CRITICAL: $i extension" . ($i > 0?"s":"") . " registered\n";
        exit $ERRORS{'CRITICAL'};
}
elsif (($i < $opt_c) && ($i >= $opt_w)){
        print "EXT WARN: $i extension" . ($i > 0?"s":"") . " registered\n";
        exit $ERRORS{'WARNING'};
}
else{
        print "EXT OK: $i extension" . ($i > 0?"s":"") . " registered\n";
        exit $ERRORS{'OK'};
}

sub print_usage () {
        print "Usage: $PROGNAME -e extension [-r realm ] [-t <timeout>] [-w <warning>] [-c <critial>] [-f <path to fs_cli>] [-v verbose]\n";
}


sub print_help () {
  
        print_revision($PROGNAME,'0.0.4');
        print "Copyright (c) 2017 Luis Daniel Lucio Quiroz <dlucio\@okay.com.mx>\n";
        print "\n";
        print_usage();
        print "\n";
        print " Checks the number of extensions registered. Sends an allert if there are too much registered.\n";
        print " Feedback/patches to support are  welcome\n\n";
        print "-e (--extension) = Extension number to verify connection, use * as a wildcard and \@ for a domain. This parameter can be specified multiple times.\n";
        print "-r (--realm) = realm/domain name of sip, if you omit then we use any\n";
        print "-t (--timeout) = Plugin timeout in seconds (default = $utils::TIMEOUT)\n";
        print "-f (--fscli) = Path to fs_cli, by default /usr/bin/fs_cli\n";
        print "-w (--warning) = Number of registered extensions to send a warning\n";
        print "-c (--critical) = Number of registered extensions to send a critical\n";
        print "-h (--help)\n";
        print "-V (--version)\n";
        print "-v (--verbose) = debugging output\n";
        print "\n\n";
}

sub process_arguments(){
        GetOptions
                ("V" => \$opt_V, "version" => \$opt_V,
                "v" => \$opt_v, "verbose" => \$opt_v,
                "h" => \$opt_h, "help" => \$opt_h,
                "e=s" => \@opt_e, "extension=s" => \@opt_e, # extension
                "r:s" => \$opt_r, "realm:s" => \$opt_r, # 
                "t:i" => \$opt_t, "timeout:i" => \$opt_t,
                "f:s" => \$opt_fs,"fscli:s" => \$opt_fs,
                "w:i" => \$opt_w,"warning:i" => \$opt_w,
                "c:i" => \$opt_c,"critical:i" => \$opt_c
        );

        if ($opt_V) {
                print_revision($PROGNAME,'0.0.1');
                exit $ERRORS{'OK'};
        }

        if ($opt_h) {
                print_help();
                exit $ERRORS{'OK'};
        }

        if (! defined($opt_c)){
                $opt_c = 5;
        }

        if (! defined($opt_w)){
                $opt_w = 10;
        }

        if ( $opt_w > $opt_c){
                print "Critical can not be less than Warning\n";
                exit $ERRORS{'UNKNOWN'};
        }
  
        if (! @opt_e){
                print "Missing extention number\n";
                exit $ERRORS{'UNKNOWN'};
        }

        if (defined $opt_v ){
                $verbose = $opt_v;
        }

        unless (defined $opt_t) {
                $opt_t = $utils::TIMEOUT ; # default timeout
        }

        if (defined $opt_fs) {
                $command = "$opt_fs --execute 'show registrations'"
        }

        return $ERRORS{'OK'};
}
